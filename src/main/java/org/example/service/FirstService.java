package org.example.service;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
public class FirstService {

//    @Scheduled(cron = "* * * * * *")
    public void firstMethod() {
        System.out.println("first method jalan gengki! " + LocalDateTime.now());
    }

    @Scheduled(cron = "0 50 20 * * *")
    public void masukKelas() {
        System.out.println("MASUK WOY");
    }
}
