package org.example.scheduler;

import org.example.service.FirstService;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

public class FirstScheduler extends QuartzJobBean {

    @Autowired
    public FirstService firstService;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {
//        System.out.println("\nscheduler is running : ");
//        firstService.firstMethod();
    }
}
